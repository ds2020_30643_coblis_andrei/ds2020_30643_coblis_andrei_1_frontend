import React from "react";
import { Link, NavLink } from "react-router-dom";

const NavBar = ({ user }) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        Online Medication Platform
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          {!user && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/login">
                Login
              </NavLink>
              <NavLink className="nav-item nav-link" to="/register">
                Register
              </NavLink>
            </React.Fragment>
          )}
          {user && user.Role === "doctor" && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/users">
                Users
              </NavLink>
              <NavLink className="nav-item nav-link" to="/patients">
                Patients
              </NavLink>
              <NavLink className="nav-item nav-link" to="/doctors">
                Doctors
              </NavLink>
              <NavLink className="nav-item nav-link" to="/caregivers">
                Caregivers
              </NavLink>
              <NavLink className="nav-item nav-link" to="/medications">
                Medications
              </NavLink>
              <NavLink className="nav-item nav-link" to="/medicationPlans">
                Medication Plans
              </NavLink>
              <NavLink className="nav-item nav-link" to="/logout">
                Logout
              </NavLink>
            </React.Fragment>
          )}
          {user && user.Role === "caregiver" && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/patients">
                Patients
              </NavLink>
              <NavLink className="nav-item nav-link" to="/caregivers">
                Caregivers
              </NavLink>
              <NavLink className="nav-item nav-link" to="/medicationPlans">
                Medication Plans
              </NavLink>
              <NavLink className="nav-item nav-link" to="/logout">
                Logout
              </NavLink>
            </React.Fragment>
          )}
          {user && user.Role === "patient" && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/patients">
                Patients
              </NavLink>
              <NavLink className="nav-item nav-link" to="/medicationPlans">
                Medication Plans
              </NavLink>
              <NavLink className="nav-item nav-link" to="/logout">
                Logout
              </NavLink>
            </React.Fragment>
          )}
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
