import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { getMedicationPlan, saveMedicationPlan } from "../services/medicationPlanService";

class MedicationPlanForm extends Form {
  state = {
    data: {
      patientId: "",
      startDate: "",
      endDate: "",
    },
    errors: {}
  };

  schema = {
    id: Joi.number(),
    patientId: Joi.number()
      .required()
      .label("PatientId"),
    startDate: Joi.date()
      .required()
      .label("StartDate"),
    endDate: Joi.date()
      .required()
      .label("EndDate"),
  };

  async populateMedicationPlan() {
    try {
      const id = this.props.match.params.id;
      if (id === "new") return;

      const { data: medicationPlan } = await getMedicationPlan(id);
      this.setState({ data: this.mapToViewModel(medicationPlan) });
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        this.props.history.replace("/not-found");
    }
  }

  async componentDidMount() {
    await this.populateMedicationPlan();
  }

  mapToViewModel(medicationPlan) {
    return {
      id: medicationPlan.id,
      patientId: medicationPlan.patientId,
      startDate: medicationPlan.startDate,
      endDate: medicationPlan.endDate,
    };
  }

  doSubmit = async () => {
    await saveMedicationPlan(this.state.data);

    this.props.history.push("/MedicationPlans");
  };

  render() {
    return (
      <div>
        <h1>MedicationPlan Form</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("patientId", "PatientId")}
          {this.renderInput("startDate", "StartDate")}
          {this.renderInput("endDate", "EndDate")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default MedicationPlanForm;
