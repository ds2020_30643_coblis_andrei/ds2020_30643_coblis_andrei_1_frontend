import React, { Component } from "react";
//import auth from "../services/authService";
import { Link } from "react-router-dom";
import Table from "./common/table";

class MedicationsTable extends Component {
  columns = [
    {
      path: "name",
      label: "Dame",
      content: medication => <Link to={`/medications/${medication.id}`}>{medication.name}</Link>
    },
    { path: "medicationPlanId", label: "MedicationPlanId" },
    { path: "dosage", label: "Dosage" },
    { path: "sideEffects", label: "SideEffects"}
  ];

  deleteColumn = {
    key: "delete",
    content: medication => (
      <button
        onClick={() => this.props.onDelete(medication)}
        className="btn btn-danger btn-sm"
      >
        Delete
      </button>
    )
  };

  constructor() {
    super();
    //const user = auth.getCurrentUser();
    //if (user && user.isAdmin) this.columns.push(this.deleteColumn);
    this.columns.push(this.deleteColumn);
  }

  render() {
    const { medications, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={medications}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default MedicationsTable;
